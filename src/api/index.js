import axios from '@/lib/axios'
import qs from 'qs'

//情报图片上传
export function uploadImage(data) {
    return axios({
        method: 'post',
        // headers: { 'Content-Type': 'multipart/form-data' },
        url: `${apiConfig}/upload/image`,
        data
    });

}
//情报视频上传
export function uploadVideo(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/upload/video`,
        data
    });

}
//情报信息上传
export function submit(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/submit`,
        data
    });

}
//时间轴获取
export function selectByDate(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/selectByDate`,
        data
    });

}
//日历月份查询
export function selectByMonth(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/selectByMonth`,
        data
    });

}
//图像识别算法
export function imgCall(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/imgCall`,
        data
    });

}
//视频识别算法
export function videoCall(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/videoCall`,
        data
    });

}
//视频识别算法---算法算法
export function captioning(data) {
    return axios({
        method: 'post',
        url: `${apiConfigXy}/captioning`,
        data
    });

}
//获取位置
export function getArea(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/getArea`,
        data
    });

}
//大屏查询
export function getCount(data) {
    return axios({
        method: 'post',
        url: `${apiConfig}/getCount`,
        data
    });

}