import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import statistics from '../views/statistics.vue'

const routerPush = VueRouter.prototype.push;
VueRouter.prototype.push = function (location) {
  return routerPush.call(this, location).catch(err => { })
}

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    // redirect: '/index',
    // meta: {
    //   title: '关于'
    // }
  },
  {
    path: '/statistics',
    component: statistics,
    name: 'statistics',
  },
]
const router = new VueRouter({
  routes
})

router.beforeEach((to, form, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  } else {
    document.title = '新疆采报' //此处写默认的title
  }
  next()
})

export default router
